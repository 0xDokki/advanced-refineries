package diddi.advancedrefineries.block

import diddi.advancedrefineries.block.base.{BlockARBase, ItemBlockARBase}
import diddi.advancedrefineries.{AdvancedRefineries, Constants}
import net.minecraft.block.Block
import net.minecraft.block.material.{MapColor, Material}
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.{Item, ItemStack}
import net.minecraft.util.ResourceLocation
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent

import scala.collection.mutable.ListBuffer

@Mod.EventBusSubscriber
object BlockManager {
    var registeredBlocks: ListBuffer[BlockARBase] = ListBuffer()

    val blockConveyor: BlockConveyor = new BlockConveyor
    val blockCrusherController: BlockCrusherController = new BlockCrusherController
    val blockHeavyStructure: BlockHideable = new BlockHideable(Material.ANVIL, MapColor.IRON, Constants.Block.blockHeavyStructure)
    val blockLightStructure: BlockHideable = new BlockHideable(Material.IRON, MapColor.IRON, Constants.Block.blockLightStructure)
    val blockActuator: BlockHideable = new BlockHideable(Material.IRON, MapColor.IRON, Constants.Block.blockActuator)
    val blockPowerLine: BlockStrut = new BlockStrut(Material.IRON, MapColor.IRON, Constants.Block.blockPowerLine)

    def init(): Unit = {

        registeredBlocks += blockConveyor
        registeredBlocks += blockCrusherController
        registeredBlocks += blockHeavyStructure
        registeredBlocks += blockLightStructure
        registeredBlocks += blockActuator
        registeredBlocks += blockPowerLine

        for (block: BlockARBase <- registeredBlocks) {
            registerBlock(block, block.getItemBlock)
            registerRendering(block)
        }

        MinecraftForge.EVENT_BUS.register(this.getClass)
    }

    private def registerBlock(block: BlockARBase, itemBlock: ItemBlockARBase): Unit = {
        block.setUnlocalizedName(AdvancedRefineries.MOD_ID + '.' + block.getName)
        block.setRegistryName(AdvancedRefineries.MOD_ID, block.getName)

        itemBlock.setRegistryName(block.getRegistryName)
    }

    private def registerRendering(block: BlockARBase): Unit = {
        AdvancedRefineries.proxy.addItemRenderer(new ItemStack(block), new ResourceLocation(AdvancedRefineries.MOD_ID, block.getName))
    }

    @SubscribeEvent
    def registerBlocks(evt: RegistryEvent.Register[Block]): Unit = {
        evt.getRegistry.registerAll(registeredBlocks: _*)

        // todo: make own creative tab for these to go to
        registeredBlocks.foreach(b => b.setCreativeTab(CreativeTabs.BUILDING_BLOCKS))
    }

    @SubscribeEvent
    def registerItems(evt: RegistryEvent.Register[Item]): Unit = {
        evt.getRegistry.registerAll(registeredBlocks.map(b => b.getItemBlock): _*)

        // it seems itemblock renderers need to be registered manually now
        registeredBlocks.foreach(b => {
            AdvancedRefineries.proxy.addItemRenderer(
                new ItemStack(b.getItemBlock),
                new ResourceLocation(AdvancedRefineries.MOD_ID, b.getName))
        })
    }

}
