package diddi.advancedrefineries.block

import net.minecraft.block.material.{MapColor, Material}
import net.minecraft.block.properties.PropertyBool
import net.minecraft.block.state.{BlockStateContainer, IBlockState}
import net.minecraft.entity.EntityLivingBase
import net.minecraft.item.ItemStack
import net.minecraft.util.EnumFacing
import net.minecraft.util.math.BlockPos
import net.minecraft.world.{IBlockAccess, World}
import diddi.advancedrefineries.block.base.BlockARBase

object BlockHideable {

    final val HIDDEN: PropertyBool = PropertyBool.create("hidden")

}

class BlockHideable(mat: Material, color: MapColor, name: String) extends BlockARBase(mat, color, name) {

    this.setDefaultState(this.getDefaultState.withProperty(BlockHideable.HIDDEN, false.asInstanceOf[java.lang.Boolean]))

    override def onBlockPlacedBy(worldIn: World, pos: BlockPos, state: IBlockState, placer: EntityLivingBase, stack: ItemStack): Unit = {
        val state = this.getDefaultState.withProperty(BlockHideable.HIDDEN, false.asInstanceOf[java.lang.Boolean])
        worldIn.setBlockState(pos, state)
    }

    override def isOpaqueCube(state: IBlockState): Boolean = !state.getValue(BlockHideable.HIDDEN)

    override def isFullBlock(state: IBlockState): Boolean = !state.getValue(BlockHideable.HIDDEN)

    override def getMetaFromState(state: IBlockState): Int = 0

    override def isPassable(worldIn: IBlockAccess, pos: BlockPos): Boolean = false

    // todo: IBlockState.getBlockFaceShape is the replacement here, but where to put that?
    // override def isBlockSolid(worldIn: IBlockAccess, pos: BlockPos, side: EnumFacing): Boolean = !worldIn.getBlockState(pos).getValue(BlockHideable.HIDDEN)

    override def canPlaceTorchOnTop(state: IBlockState, world: IBlockAccess, pos: BlockPos): Boolean = !state.getValue(BlockHideable.HIDDEN)

    override def createBlockState(): BlockStateContainer = new BlockStateContainer(this, BlockHideable.HIDDEN)

}
