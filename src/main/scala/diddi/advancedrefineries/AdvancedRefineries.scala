package diddi.advancedrefineries

import diddi.advancedrefineries.block.BlockManager
import diddi.advancedrefineries.config.ConfigurationHandler
import diddi.advancedrefineries.crafting.CraftingManager
import diddi.advancedrefineries.gui.GuiHandler
import diddi.advancedrefineries.item.{ItemManager, MaterialManager}
import diddi.advancedrefineries.proxy.CommonProxy
import diddi.advancedrefineries.tile.TileEntityManager
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}
import net.minecraftforge.fml.common.{Mod, SidedProxy}
import org.apache.logging.log4j.{LogManager, Logger}

@Mod(modid = AdvancedRefineries.MOD_ID, name = AdvancedRefineries.MOD_NAME, version = AdvancedRefineries.VERSION,
    modLanguage = AdvancedRefineries.LANG, acceptedMinecraftVersions = AdvancedRefineries.MC_VERSION)
object AdvancedRefineries {

    final val MOD_NAME = "Advanced Refineries"
    final val MOD_ID = "advancedrefineries"
    final val VERSION = "1.11.2-0.0.0"
    final val LANG = "scala"
    final val MC_VERSION = "1.11.2"

    @SidedProxy(clientSide = "diddi.advancedrefineries.proxy.ClientProxy",
        serverSide = "diddi.advancedrefineries.proxy.CommonProxy")
    var proxy: CommonProxy = _

    var LOGGER: Logger = LogManager.getLogger(MOD_NAME)

    @EventHandler
    def preInit(event: FMLPreInitializationEvent): Unit = {
        AdvancedRefineries.LOGGER.info("Starting PreInit...")

        ConfigurationHandler.init(event.getSuggestedConfigurationFile)
        BlockManager.init()
        MaterialManager.init()
        ItemManager.init()
        TileEntityManager.init()
        proxy.preInit(event)

        AdvancedRefineries.LOGGER.info("Finished PreInit.")
    }

    @EventHandler
    def init(event: FMLInitializationEvent): Unit = {
        AdvancedRefineries.LOGGER.info("Starting Init...")

        CraftingManager.init()
        GuiHandler.init()
        proxy.init(event)

        AdvancedRefineries.LOGGER.info("Finished Init.")
    }

    @EventHandler
    def postInit(event: FMLPostInitializationEvent): Unit = {
        AdvancedRefineries.LOGGER.info("Starting PostInit...")
        proxy.postInit(event)
        AdvancedRefineries.LOGGER.info("Finished PostInit.")
    }

}
