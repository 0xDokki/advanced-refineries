package diddi.advancedrefineries.config

import java.io.File

import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import diddi.advancedrefineries.AdvancedRefineries
import diddi.advancedrefineries.config.values.{ConfigIntValues, ConfigStringListValues}

object ConfigurationHandler {

    var config: Configuration = _

    def init(configFile: File): Unit = {

        MinecraftForge.EVENT_BUS.register(this)

        config = new Configuration(configFile)
        config.load()

        redefineConfigs()
    }

    def redefineConfigs(): Unit = {

        ConfigStringListValues.values() foreach (conf => conf.currentValues = config.get(conf.category, conf.name, conf.defaultValues, conf.desc).getStringList)

        ConfigIntValues.values() foreach (conf => conf.currentValue = config.get(conf.category, conf.name, conf.defaultValue, conf.desc).getInt)

        if (config.hasChanged)
            config.save()
    }

    @SubscribeEvent
    def onConfigurationChanged(event: OnConfigChangedEvent): Unit = {
        if (event.getModID.equalsIgnoreCase(AdvancedRefineries.MOD_ID))
            redefineConfigs()
    }

}
