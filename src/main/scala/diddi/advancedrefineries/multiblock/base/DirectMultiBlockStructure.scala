package diddi.advancedrefineries.multiblock.base

import net.minecraft.block.Block
import net.minecraft.init.Blocks

abstract class DirectMultiBlockStructure extends MultiBlockStructure {

    /**
      * ()()() -> (y)(x)(z)
      */
    val matrix: Array[Array[Array[Block]]]

    override def deriveActualMatrix(): Matrix = {
        for (y <- matrix.indices) {
            for (x <- matrix(y).indices) {
                for (z <- matrix(y)(x).indices) {
                    if (matrix(y)(x)(z) == null)
                        matrix(y)(x)(z) = Blocks.AIR
                }
            }
        }
        matrix
    }
}
