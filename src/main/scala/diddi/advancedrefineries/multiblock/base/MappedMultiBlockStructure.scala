package diddi.advancedrefineries.multiblock.base

import net.minecraft.block.Block

abstract class MappedMultiBlockStructure extends MultiBlockStructure {

    val blockMapping: Map[String, Block]
    val structure: List[String]

    // the length of a row  in the structure
    val width: Int
    // the amount of rows in a layer
    val depth: Int

    override def deriveActualMatrix(): Matrix = {
        if (structure.length % (width * depth) != 0)
            throw new Error("Invalid structure size. Expected multiple of %d, got %d".format(width * depth, structure.length))
        val blocks = structure.map(s => blockMapping(s)).toArray

        blocks.grouped(width).toArray.grouped(depth).toArray
    }

}
