package diddi.advancedrefineries.gui

import net.minecraft.entity.player.EntityPlayer
import net.minecraft.world.World
import net.minecraftforge.fml.common.network.{IGuiHandler, NetworkRegistry}
import diddi.advancedrefineries.AdvancedRefineries
import diddi.advancedrefineries.gui.client.GuiHandbook

object GuiHandler extends IGuiHandler {

    final val HANDBOOK_ID = 0

    def init(): Unit = {
        NetworkRegistry.INSTANCE.registerGuiHandler(AdvancedRefineries, GuiHandler)
    }

    override def getClientGuiElement(ID: Int, player: EntityPlayer, world: World, x: Int, y: Int, z: Int): AnyRef = {
        ID match {
            case HANDBOOK_ID =>
                new GuiHandbook
            case _ => null
        }
    }

    override def getServerGuiElement(ID: Int, player: EntityPlayer, world: World, x: Int, y: Int, z: Int): AnyRef = {
        null
    }

}
