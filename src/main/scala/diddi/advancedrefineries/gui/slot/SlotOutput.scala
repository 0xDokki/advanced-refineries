package diddi.advancedrefineries.gui.slot

import net.minecraft.inventory.{IInventory, Slot}
import net.minecraft.item.ItemStack

class SlotOutput(iInventory: IInventory, index: Int, x: Int, y: Int) extends Slot(iInventory, index, x, y) {
    override def isItemValid(stack: ItemStack): Boolean = false
}
