package diddi.advancedrefineries.gui.client

import diddi.advancedrefineries.AdvancedRefineries
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.resources.I18n
import net.minecraft.util.ResourceLocation
import net.minecraft.util.text.TextFormatting

class GuiHandbook extends GuiScreen {

    private val resLoc =
        new ResourceLocation(AdvancedRefineries.MOD_ID, "textures/gui/handbook.png")

    private val pageSize = (132, 180)
    private val paperSize = (124, 168)

    private var basePos = (0, 0)

    override def initGui(): Unit = {
        basePos = ((width / 2) - pageSize._1, (height - pageSize._2) / 2)
    }

    override def drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float): Unit = {
        this.drawDefaultBackground()
        // draw the book in the smaller unicode font, but remember the previous setting
        val wasUnicode = this.fontRenderer.getUnicodeFlag
        this.fontRenderer.setUnicodeFlag(true)

        this.mc.getTextureManager.bindTexture(resLoc)
        // cover
        this.drawTexturedModalRect(basePos._1, basePos._2, 0, 0, pageSize._1, pageSize._2)
        this.drawTexturedModalRect(basePos._1 + pageSize._1, basePos._2, 0, 0, pageSize._1, pageSize._2)

        val leftPaperPos = (basePos._1 + 8, basePos._2 + 6);
        val rightPaperPos = (basePos._1 + pageSize._1, basePos._2 + 6)
        // paper
        this.drawTexturedModalRect(leftPaperPos._1, leftPaperPos._2, 132, 0, paperSize._1, paperSize._2)
        this.drawTexturedModalRect(rightPaperPos._1, rightPaperPos._2, 132, 0, paperSize._1, paperSize._2)

        this.fontRenderer.drawSplitString(parseIdentifier("lorem_ipsum"), leftPaperPos._1 + 3, leftPaperPos._2 + 3, 120, 0)
        this.fontRenderer.drawSplitString(parseIdentifier("lorem_ipsum"), rightPaperPos._1 + 3, rightPaperPos._2 + 3, 120, 0)

        this.fontRenderer.setUnicodeFlag(wasUnicode)
    }

    override def onResize(mcIn: Minecraft, w: Int, h: Int): Unit = {
        super.onResize(mcIn, w, h)
        basePos = ((w / 2) - pageSize._1, (h - pageSize._2) / 2)
    }

    def parseIdentifier(identifier: String): String = {
        val id = "handbook." + AdvancedRefineries.MOD_ID + "." + identifier
        var str = I18n.format(id)
        str = str.replaceAll("<br>", "\n")

        str = str.replaceAll("<f>", TextFormatting.BOLD + "")
        str = str.replaceAll("<i>", TextFormatting.ITALIC + "")
        str = str.replaceAll("<u>", TextFormatting.UNDERLINE + "")

        str = str.replaceAll("<r>", TextFormatting.DARK_RED + "")
        str = str.replaceAll("<g>", TextFormatting.DARK_GREEN + "")
        str = str.replaceAll("<b>", TextFormatting.DARK_BLUE + "")

        str = str.replaceAll("<rs>", TextFormatting.BLACK + "")
        str + TextFormatting.WHITE
    }
}
