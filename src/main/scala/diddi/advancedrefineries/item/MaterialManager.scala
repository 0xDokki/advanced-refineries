package diddi.advancedrefineries.item

import java.util.Locale

import diddi.advancedrefineries.AdvancedRefineries
import diddi.advancedrefineries.config.values.ConfigStringListValues
import diddi.advancedrefineries.item.base.ItemARBase
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.oredict.OreDictionary

import scala.collection.mutable

@Mod.EventBusSubscriber
object MaterialManager {

    val materials: mutable.HashMap[String, ItemARBase] = new mutable.HashMap[String, ItemARBase]()

    def init(): Unit = {
        AdvancedRefineries.LOGGER.info("Initializing Materials...")

        ConfigStringListValues.MATERIALS.currentValues.foreach(mat => {
            AdvancedRefineries.LOGGER.info("Generating Material " + mat)

            val ingot_name = "ingot" + mat
            val dust_name = "dust" + mat
            val nugget_name = "nugget" + mat

            if (OreDictionary.getOres(ingot_name, false).size() < 1) {
                AdvancedRefineries.LOGGER.info("Creating ingot for Material " + mat)
                materials += (("ingot" + mat) -> createIngot(mat))
            }
            if (OreDictionary.getOres(dust_name, false).size() < 1) {
                AdvancedRefineries.LOGGER.info("Creating dust for Material " + mat)
                materials += (("dust" + mat) -> createDust(mat))
            }
            if (OreDictionary.getOres(nugget_name, false).size() < 1) {
                AdvancedRefineries.LOGGER.info("Creating nugget for Material " + mat)
                materials += (("nugget" + mat) -> createNugget(mat))
            }
        })

        AdvancedRefineries.LOGGER.info("Finished Initializing Materials.")
    }

    @SubscribeEvent
    def registerItems(evt: RegistryEvent.Register[Item]): Unit = {
        evt.getRegistry.registerAll(materials.values.toList: _*)
        materials.foreach(mat => {
            ItemManager.registerRendering(mat._2)
            OreDictionary.registerOre(mat._1, mat._2)
            mat._2.setCreativeTab(CreativeTabs.MISC)
        })
    }

    private def createIngot(name: String): ItemARBase = {
        new ItemARBase("item_ingot_" + name.toLowerCase(Locale.ROOT))
    }

    private def createDust(name: String): ItemARBase = {
        new ItemARBase("item_dust_" + name.toLowerCase(Locale.ROOT))
    }

    private def createNugget(name: String): ItemARBase = {
        new ItemARBase("item_nugget_" + name.toLowerCase(Locale.ROOT))
    }

}
