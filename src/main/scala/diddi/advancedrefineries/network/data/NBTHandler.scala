package diddi.advancedrefineries.network.data

import net.minecraftforge.fml.common.network.simpleimpl.{IMessage, IMessageHandler, MessageContext}
import net.minecraftforge.fml.relauncher.{Side, SideOnly}

object NBTHandler {

    sealed class ClientHandler extends IMessageHandler[NBTPacket, IMessage] {
        @SideOnly(Side.CLIENT)
        override def onMessage(message: NBTPacket, ctx: MessageContext): IMessage = {
            if (message.data != null && message.handler != null)
                message.handler.handleData(message.data)
            null
        }
    }

    class ServerHandler extends IMessageHandler[NBTPacket, IMessage] {
        override def onMessage(message: NBTPacket, ctx: MessageContext): IMessage = {
            if (message.data != null && message.handler != null)
                message.handler.handleData(message.data)
            null
        }
    }

}
