package diddi.advancedrefineries.proxy

import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.ItemStack
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}

import scala.collection.mutable

class ClientProxy extends CommonProxy {

    private val itemRenderers = new mutable.HashMap[ItemStack, ResourceLocation]

    override def preInit(event: FMLPreInitializationEvent): Unit = {

    }

    override def init(event: FMLInitializationEvent): Unit = {
        for ((stack: ItemStack, resLoc: ResourceLocation) <- itemRenderers) {
            Minecraft.getMinecraft.getRenderItem.getItemModelMesher.register(
                stack.getItem,
                stack.getItemDamage,
                new ModelResourceLocation(resLoc, "inventory"))
        }
    }

    override def postInit(event: FMLPostInitializationEvent): Unit = {

    }

    override def addItemRenderer(stack: ItemStack, resLoc: ResourceLocation): Unit = {
        itemRenderers.put(stack, resLoc)
    }
}
